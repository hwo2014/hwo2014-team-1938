import json
import socket
import sys
import math

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.race_data = {}
        self.track = {}
        self.pieces = []
        self.crash_position = {}
        self.last_position = {}
        self.speed = 0
        self.last_throttle = 0
        self.safe_throttle = 0.654
        self.last_piece_tick = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def joinKeimola(self):
        return self.msg("createRace", {
              "botId": {
                "name": "Antti",
                "key": "NSH+PYRGRlktGg"
              },
              "trackName": "keimola",
              "password": "schumi4ever",
              "carCount": 1
        })
    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):

        self.joinKeimola()
        self.msg_loop()

    def on_join(self, data, gameTick=-1):
        print "Joined"
        self.ping()

###
#The message sequence for an actual race is slightly different, as there's a qualifying session before the race.
#The main difference is that there are two sets of gameStart / gameEnd pairs after a single gameInit message.
##
    def on_game_init(self, data, gameTick=-1):
        self.race_data = data
        self.track = data["race"]["track"]
        self.pieces = self.track["pieces"]

        track_length = 0

        for piece in self.pieces:
            track_length += self.get_piece_length(piece)

        self.track["length"] = track_length
        self.car = data["race"]["cars"][0]
        print "Game init. Track length is: {}. Car: {}".format(track_length, self.car["dimensions"])

        self.ping()

    def get_piece_length(self, piece):
        piece_length = piece.get("length", 0)

        if piece_length == 0:
            piece_length = (math.fabs(piece["angle"])/360.00) * 2 * math.pi * piece["radius"]

        return piece_length

    def on_game_start(self, data, gameTick=-1):
        print("Race started")
        self.ping()

    def on_lap_finished(self, data, gameTick=-1):
        print "on lap finished"
        time = data["lapTime"]["millis"]/1000
        tick_speed = self.track["length"] / data["lapTime"]["ticks"]
        speed = self.track["length"] / time

        print "Lap {} finished in {}s. Tick speed: {} Length/s: {}".format(data["lapTime"]["lap"], time, tick_speed, speed)

        self.ping()

    def on_car_positions(self, data, gameTick=-1):

        piece_changed = False
        if self.last_position.get("data"):
            last_piece_index = self.last_position["data"]["piecePosition"]["pieceIndex"]
            new_piece_index = data[0]["piecePosition"]["pieceIndex"]
            if last_piece_index != new_piece_index:
                piece_changed = True
                self.last_piece_tick = self.last_position.get("tick", 0)
                length = self.get_piece_length(self.pieces[last_piece_index])
                ticks = gameTick - self.last_piece_tick
                self.speed = length/ticks
                print """Piece changed. Ticks spent on {}. Throttle: {}, ticks: {},
                 speed: {}""".format(last_piece_index, self.set_throttle, ticks, self.speed)
                self.last_position["tick"] = gameTick

        self.last_position["data"] = data[0]

        self.update_throttle()

    def update_throttle(self):
        next_piece_index = self.get_next_piece_index(self.last_position["data"]["piecePosition"]["pieceIndex"])
        next_next_piece_index = self.get_next_piece_index(next_piece_index)

        throttle = self.safe_throttle
        #print car angle in bend
        #if this and next piece has same angle it's straight?
        next_piece = self.pieces[next_piece_index]
        current_piece = self.pieces[self.last_position["data"]["piecePosition"]["pieceIndex"]]
        next_next_piece = self.pieces[next_next_piece_index]

        current_piece_angle = current_piece.get("angle", 0)
        next_piece_angle = next_piece.get("angle", 0)
        next_next_piece_angle = next_next_piece.get("angle", 0)

        #print "Angles: current: {}, next: {}, next_next: {}".format(current_piece_angle, next_piece_angle, next_next_piece_angle)

        if self.speed < 3:
            throttle = 1
        elif math.fabs(next_piece_angle) >= 24:
            if self.speed > 7:
                throttle = 0.5
            elif self.speed > 6:
                throttle = 0.58
            else:
                throttle = 0.8
        elif math.fabs(next_next_piece_angle) > 24:
            if self.speed > 8:
                throttle = 0.5
            elif self.speed > 0.7:
                throttle = 0.58
            else:
                throttle = 0.8
        elif math.fabs(current_piece_angle) > 24:
            if self.speed > 7:
                throttle = 0.55
            elif self.speed > 6:
                throttle = 0.65
            else:
                throttle = 0.9
        elif math.fabs(current_piece_angle) < 20 and math.fabs(next_piece_angle) < 20 and math.fabs(next_next_piece_angle) < 20:
            throttle = 0.93

        elif math.fabs(current_piece_angle) < 20 and math.fabs(next_piece_angle) < 20:
            throttle = 0.85
        elif math.fabs(next_piece_angle) < 20 and math.fabs(next_next_piece_angle) < 20:
            self.speed = 0.9
        else:
            if self.speed > 8.5:
                throttle = 0.68
            else:
                throttle = 0.8

        self.set_throttle = throttle
        self.throttle(throttle)

    def get_next_piece_index(self, current_index):
        if current_index == len(self.pieces)-1:
            return 0
        else:
            return current_index + 1

    def on_crash(self, data, gameTick=-1):
        print "Crash. Throttle was: {}".format(self.set_throttle)
        #print "crash car angle: {}, bend angle {}".format(self.last_position["data"]["angle"], self.pieces[self.last_position["data"]["piecePosition"]["pieceIndex"].get("angle", 0)])
        self.ping()

    def on_game_end(self, data, gameTick=-1):
        print "Race ended Position"
        self.ping()

    def on_error(self, data, gameTick=-1):
        print "Error: {0}".format(data)
        self.ping()

    def on_dnf(self, data, gameTick=-1):
        print "DNF: {0}".format(data)
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'lapFinished': self.on_lap_finished,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'dnf': self.on_dnf
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            game_tick = msg.get('gameTick')
            if msg_type in msg_map:
                if game_tick:
                    msg_map[msg_type](data, game_tick)
                else:
                    msg_map[msg_type](data)
            else:
                print "Got {0}".format(msg_type)
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print "Usage: ./run host port botname botkey"
    else:
        host, port, name, key = sys.argv[1:5]
        print "Connecting with parameters:"
        print "host={0}, port={1}, bot name={2}, key={3}".format(host, port, name, key)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
